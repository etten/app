<?php

namespace Etten\App\Tests;

use PHPUnit;

abstract class TestCase extends PHPUnit\Framework\TestCase
{

	public function __get($name)
	{
		$getter = 'get' . ucfirst($name);
		if (method_exists($this, $getter)) {
			return $this->$getter();
		}

		throw new \RuntimeException(sprintf('Property %s does not found.', $name));
	}

}
